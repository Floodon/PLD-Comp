Manuel d’utilisation du compilateur

*********************************************************************************************************
# Récuper le projet depuis GitLab

Utiliser la commande: `git clone https://gitlab.com/Floodon/PLD-Comp.git`


# Compiler le compilateur

(mais qui compile le compilateur du compilateur ? ;)

Ouvrir l’invité de commande. 
Se placer dans le dossier PLD-Comp
Se placer dans le dossier compiler (cd compiler)
- Utiliser la commande `./compile_if` (si vous êtes sur un ordinateur du département)
- Utiliser la commande `./compile_ubuntu` (si vous êtes sur un ordinateur sous ubuntu)
- Utiliser la commande `./compile_docker` (si vous utilisez la configuration docker du projet)


# Lancer des tests sans recompiler le projet

Depuis la racine du projet faire `cd tests`.
Supprimer les résultats des tests précédents avec la commande `rm -rf pld-test-output`.
Lancer les tests de votre choix avec la commande `python3 pld-test.py tests/[chemin_vers_le_répertoire_de_test]`.
En partant à la racine du projet, vous trouverez les répertoires de tests dans le répertoire tests/Init.
Si vous ne précisez pas de répertoire des tests, l’ensemble des tests est effectué.


# Compiler et lancer des tests

Depuis la racine du projet faire `cd compiler`.
Sur une machine fonctionnement sur ubuntu, lancez la commande `./do-it.sh [/chemin_un_repertoire_de_test]`.
Sur une machine du département, lancez la commande `./do-it-if.sh [/chemin_un_repertoire_de_test]`

Ce scripte compile notre compilateur puis exécute les tests associés au répertoire de test correspondant. Si vous ne mentionnez aucun répertoire de test, l’ensemble des tests sera réalisé.



