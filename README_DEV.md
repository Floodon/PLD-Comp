# Project Compilateur

Compilateur d'un sous ensemble du langage `C`, développé en `C++` 

Développé par l'Hexanôme H4241 : 
* Pierre-Louis Jallerat
* Alexandre Bonhomme
* Antoine Mandin
* Damien Carreau
* Enzo Boscher
* Timothé Berthier

## Librairie

Notre compilateur s'appuie sur la librairie `antlr4` qui, en se basant une grammaire (`ifcc.g4`), auto-génère un *parser* capable de construire un arbre de règles à partir d'un programme en `C`. Le compilateur parcours cet arbre pour générer le code assembleur correspondant.

## Fonctionnalités 

Prend en charge un fichier `main.c` contenant potentiellement plusieurs fonctions sans paramètres et retournant un entier, suivies d'une fonction `int main() {}`.

Elles consistent en une suite d'instructions : déclaration, affectation, condition, boucle, bloc, return, expression.

On ne supporte que les variables entières `int`.

### Visite statique

Nous utilisons un visiteur statique (pré-processeur), qui vérifie la validité du code (double déclaration de variable, utilisation d'une variable non déclarée, etc).

Il permet aussi de compter la mémoire nécessaire à chaque fonction.

### Déclaration - Affectation 

* Plusieurs déclaration à différents endroits sont possibles.
* Les affectations sont possibles à la déclaration
* Plusieurs déclarations (et affecations) sont possibles sur une même ligne (séparées par `,`)

### Expressions

Les expressions supportées actuellement sont : 
* Addition, soustraction
* Multiplication, division, modulo
* Parenthèses
* Opérateur moins unaire (`-a`)
* Comparaisons (`==`,`!=`,`>=`,`<=`,`<`,`>`)
* Opérateurs bit à bit (`&`, `|`, `^`)
* Opérateur non unaire (`!`)

### Conditions 

* Les conditions sont possibles partout dans le code
* Elles prennent une expression en paramètre 
* Il est possible de les imbriquer
* Les accolades sont optionnelles (`if(1) a = b;` possible)

### Boucles 

Les boucles `while` et `for` sont prises en charge. 

### Fonctions 

Les fonctions personnalisées, sans paramètre et retournant un entier sont possibles. 

Elles doivent être déclarées avant le `main`.

L'appel de fonction est possible partout dans le code, y compris dans les fonctions elles-mêmes.

La gestion des variables interdit pour l'instant l'utilisation de deux variables du même nom dans deux fonctions différentes. 

On peut faire appel à la fonction standard de la librairie `C` `putchar()`.

### Tableaux

Le compilateur accèpte les tableaux d'entiers. 
La déclaration d'un tableau necessite une taille constante (ou une expressions de constantes, ex : `tab[4*3+6]`).

L'accès et l'affectation peut se faire à des indices constants, variables ou résultats d'expressions. (ex : `tab1[2*a-7] = -5;`)


## Structure 

Le dossier `compiler` contient :
* Le code auto-généré par antlr4, dans `antlr4-generated`
* Le code compilé dans le dossier `output`
* Plusieurs scripts shell pour compiler le code
* La grammaire utilisée dans le fichier `ifcc.g4`
* Le fichier principal du programme, `main.cpp`
* Le `MAKEFILE`
* Un script `do-it.sh` permettant de compiler le programme et lancer les tests
    * Si utilisé sans argument --> Lance tous les tests
    * Si utilisé avec un argument --> Lance les tests du sous dossier spécifié en argument 
* Les fichiers du visiteur
    * `visitor.h`
    * `visitor.cpp`
* Les fichiers du visiteur statique
    * `staticVisitor.h`
    * `staticVisitor.cpp`
* Les fichiers de la classe `ExpressionResult`, utilisée pour l'optimisation des expressions
    * `expressionResult.h`
    * `expressionResult.cpp`


La classe `Visitor` contient : 
* une liste de variables dans une `Map`, liant leur nom à leur addresse mémoire
* plusieurs méthodes de visite de l'arbre
* la liste des fonctions déclarées
* une `Map` liant chaque fonction à la quantité de mémoire dont elle a besoin
* Le nombre de variables affectées (s'incrémentant au cours de l'exécution)
* Le nombre de variables temporaires utilisées (pour le calcul des expressions)
* Le nombre de labels utilisés (pour les conditions et boucles)

La classe `StaticVisitor` contient : 
* Une structure de données (`map` de `map`) comptant, pour chaque fonction, les variables déclarées et leur taille en mémoire
* Une structure de données (`map` de `set`) comptant, pour chaque fonction, les variables affectées
* Le nom courrant de la fonction visitée (qui évolue au cours de la visite)
* Une liste des erreurs
* Une liste des warnings

La classe `ExpressionResult` représente le résultat d'une expression. Elle contient donc : 
* Un type : Constante, Variable ou Registre si le résultat est stocké dans le registre `eax`
* Une valeur :
    * Si le type est *constante*, la valeur de la constante
    * Si le type est *variable*, l'adresse de la variable
    * Si le type est *registre*, la valeur est inutile

Le dossier `tests` contient les tests et leurs sorties. Les tests sont organisés par catégorie, en sous-dossiers.
Le dossier `tests/tests/Init` contient les code `C` qui vont être compilés par notre programme et `gcc` pour vérifier le comportement de notre compilateur.
Les sorties des compilations sont dans le dossier `test/pld-test-output`.

