int fonctionB(){
    putchar(23);
	return 2;
}

int fonctionA(){
    int c = fonctionB();
	return c;
}

int fonctionC(){
    int w = 4;
    int x = w + fonctionA();
    return x;
}

int main(){
	int a = fonctionA()+fonctionB();
    int b = a + fonctionC();
	return a;
}
