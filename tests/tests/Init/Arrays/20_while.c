int main(){
    int a = 0;
    int tab[4];

    tab[0] = 2345;
    tab[1] = 543;
    tab[2] = -56;
    tab[3] = -789;

    int sum = 0;
    while(a < 4){
        sum = sum + tab[a];
        a = a + 1;
    }
    return sum;
}