int main()
{
    int tab1[2], tab2[2];
    int a = 13, b = 16;
    tab1[0] = a * b;
    tab1[1] = 15;
    tab2[0] = tab1[1] * a - 7 * (5 - tab1[0]);
    tab2[tab2[0] * 0 + 1] = 3;

    return tab1[0] + tab1[1] + tab2[0] + tab2[1];
}