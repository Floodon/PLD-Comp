#pragma once

#include<string>
using namespace std;

const int TYPE_CONST = 0, TYPE_VARIABLE = 1, TYPE_REGISTER = 2;

class ExpressionResult {

    public :
        ExpressionResult(int mType, int mValue) : type(mType), value(mValue) {}
        int getType() {return type;}
        int getValue() {return value;}
        void setType(int type) {this->type = type;}
        void setValue(int value) {this->value = value;}
        string toString();

    private :
        int type;
        int value;
};