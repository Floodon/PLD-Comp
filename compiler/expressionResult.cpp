#include "expressionResult.h"
using namespace std; 


string ExpressionResult::toString() {
    switch(type){
        case TYPE_CONST: 
            return "{Const:"+to_string(value)+"}";
        case TYPE_VARIABLE:
            return "{Var:"+to_string(value)+"}";
        case TYPE_REGISTER:
            return "{Register}";
    }
    return "{UNKNOWN}";
}