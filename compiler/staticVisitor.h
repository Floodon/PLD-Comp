
// Copied from a file generated from ifcc.g4 by ANTLR 4.7.2

#pragma once

#include "antlr4-runtime.h"
#include "antlr4-generated/ifccVisitor.h"
#include "antlr4-generated/ifccBaseVisitor.h"
#include <map>
#include <unordered_set>
#include <list>
#include <string>
using namespace std;

/**
 * This class provides an empty implementation of ifccVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class StaticVisitor : public ifccBaseVisitor
{
public:
	virtual ~StaticVisitor();
	virtual antlrcpp::Any visitProg(ifccParser::ProgContext *ctx) override;
	virtual antlrcpp::Any visitUnaryLValueExpAffectation(ifccParser::UnaryLValueExpAffectationContext *ctx) override;
	virtual antlrcpp::Any visitUnaryLValueAffectation(ifccParser::UnaryLValueAffectationContext *ctx) override;
	virtual antlrcpp::Any visitLValue(ifccParser::LValueContext *ctx) override;
	virtual antlrcpp::Any visitDeclaration(ifccParser::DeclarationContext *context) override;
	virtual antlrcpp::Any visitAffectation(ifccParser::AffectationContext *ctx) override;
	virtual antlrcpp::Any visitVarExpression(ifccParser::VarExpressionContext *ctx) override;
	virtual antlrcpp::Any visitFunction(ifccParser::FunctionContext *ctx) override;
	virtual antlrcpp::Any visitExpression(ifccParser::ExpressionContext *ctx) override;
	virtual antlrcpp::Any visitNotExpression(ifccParser::NotExpressionContext *ctx) override;
	virtual antlrcpp::Any visitConstExpression(ifccParser::ConstExpressionContext *ctx) override;
	virtual antlrcpp::Any visitArrayExpression(ifccParser::ArrayExpressionContext *ctx) override;
	virtual antlrcpp::Any visitParExpression(ifccParser::ParExpressionContext *ctx) override;
	virtual antlrcpp::Any visitFonctionExpression(ifccParser::FonctionExpressionContext *ctx) override;

	bool printErrors();
	void printWarnings();
	map<string, int> memoryNeededPerFunction();

private:
	int checkDeclarationExpression(ifccParser::LValueContext *ctx);

	map<string, map<string, int>> declaredVariablesPerFunction;
	map<string, unordered_set<string> *> affectedVariablesPerFunction;
	vector<string> functions; //TODO use an unordered set, and 2 (declared + used)
	string currentFunctionName;
	list<string> errors;
	list<string> warnings;
};
