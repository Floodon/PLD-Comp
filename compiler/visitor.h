
// Copied from a file generated from ifcc.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "antlr4-generated/ifccVisitor.h"
#include "antlr4-generated/ifccBaseVisitor.h"
#include <map>
#include <list>
#include <string>
using namespace std;

/**
 * This class provides an empty implementation of ifccVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  Visitor : public ifccBaseVisitor {
public:
	Visitor(map<string, int> memoryNeeded) : memoryNeededPerFunction(memoryNeeded) {}
	virtual antlrcpp::Any visitProg(ifccParser::ProgContext *ctx) override;
	virtual antlrcpp::Any visitMain(ifccParser::MainContext *ctx) override;
	virtual antlrcpp::Any visitCondition(ifccParser::ConditionContext *context) override;
	virtual antlrcpp::Any visitUnaryLValueExpAffectation(ifccParser::UnaryLValueExpAffectationContext *ctx) override;
	virtual antlrcpp::Any visitUnaryLValueAffectation(ifccParser::UnaryLValueAffectationContext *ctx) override;
    virtual antlrcpp::Any visitDeclaration(ifccParser::DeclarationContext *context) override;
    virtual antlrcpp::Any visitDeclarationFonction(ifccParser::DeclarationFonctionContext *context) override;
	virtual antlrcpp::Any visitAppelFonction(ifccParser::AppelFonctionContext *context) override;
	virtual antlrcpp::Any visitFonctionExpression(ifccParser::FonctionExpressionContext *context) override;
	virtual antlrcpp::Any visitAffectation(ifccParser::AffectationContext *ctx) override;
	virtual antlrcpp::Any visitRet(ifccParser::RetContext *ctx) override;
	virtual antlrcpp::Any visitFunction(ifccParser::FunctionContext *ctx) override;
	virtual antlrcpp::Any visitExpression(ifccParser::ExpressionContext *ctx) override;
	virtual antlrcpp::Any visitParExpression(ifccParser::ParExpressionContext *ctx) override;
	virtual antlrcpp::Any visitConstExpression(ifccParser::ConstExpressionContext *ctx) override;
	virtual antlrcpp::Any visitVarExpression(ifccParser::VarExpressionContext *ctx) override;
	virtual antlrcpp::Any visitArrayExpression(ifccParser::ArrayExpressionContext *ctx) override;
	virtual antlrcpp::Any visitLValue(ifccParser::LValueContext *ctx) override;
	virtual antlrcpp::Any visitNotExpression(ifccParser::NotExpressionContext *ctx) override;
 	virtual antlrcpp::Any visitLoopWhile(ifccParser::LoopWhileContext *ctx) override;
 	virtual antlrcpp::Any visitLoopFor(ifccParser::LoopForContext *ctx) override;
private:
	map<string, int> variables; 
	list<string> functions; // A revisiter TODO
	int varCount = 0;
	int tempCount = 0;
	int nbIf = 0;
	map<string, int> memoryNeededPerFunction;
};

