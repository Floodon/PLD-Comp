// Copied from a file generated from ifcc.g4 by ANTLR 4.7.2

#include "staticVisitor.h"
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <algorithm>

using namespace std;

// ==== Constants

const string REGISTER_RESULT = "eax";
const string REGISTER_1 = "edx";
const string REGISTER_2 = "rdx";
const string REGISTER_FCT = "edi";

/*
	TODO : Division per 0 => Warning
	TODO : Remove errors and warning of main visitor
*/

// ==== Functions

/**
 * Write a message into the Logcat (cerr here)
 */
void staticLog(string message)
{
	cerr << "StaticVisitor: " << message << endl;
}

string charPosition(antlr4::Token *token)
{
	return to_string(token->getLine()) + ":" + to_string(token->getCharPositionInLine());
}

string error(string message, antlr4::Token *problematicToken)
{
	return charPosition(problematicToken) + " error: " + message;
}

string warning(string message, antlr4::Token *problematicToken)
{
	return charPosition(problematicToken) + " warning: " + message;
}

string warning(string message)
{
	return "warning: " + message;
}

int StaticVisitor::checkDeclarationExpression(ifccParser::LValueContext *ctx)
{
	staticLog("checkDeclarationExpression");

	auto exp = ctx->expression();
	if (exp != NULL)
	{
		if (exp->getText().find_first_not_of("0123456789") != std::string::npos)
		{
			if (exp->getText().find_first_not_of("0123456789*/+-|^&!=><") != std::string::npos)
			{
				int answer = visitExpression(exp).as<int>();
				return 4 * answer;
			}
			else
			{
				staticLog("Déclaration d'un tableau avec une taille variable");
				errors.push_back(error("Array declaration with variable size '" +
										   ctx->IDENT()->getText() + "'",
									   ctx->IDENT()->getSymbol()));
			}
			/*Consider using : 
			#include "tinyexpr.h"
			#include <stdio.h>

			int main()
			{
				double answer = te_interp("3*2+4*1+(4+9)*6", 0);
				printf("Answer is %f\n", answer);
				return 0;
			}
			https://stackoverflow.com/questions/9329406/evaluating-arithmetic-expressions-from-string-in-c
			*/

			return 0;
		}
		else
		{
			return 4 * stoi(exp->getText()); // Return the amount of memory
		}
	}
	return 4;
}

// === Inherited Functions ===
antlrcpp::Any StaticVisitor::visitProg(ifccParser::ProgContext *ctx)
{
	staticLog("visitProgStatic");

	// Visit main as a function (we must do that for each function)
	currentFunctionName = "main";
	unordered_set<string> *affectedVariablesForMain = new unordered_set<string>();
	affectedVariablesPerFunction.insert(make_pair(currentFunctionName, affectedVariablesForMain));

	// Visit block of main
	visitChildren(ctx);

	// Check unused variables
	for (pair<string, int> declaredVariable : declaredVariablesPerFunction[currentFunctionName])
	{
		if (affectedVariablesForMain->find(declaredVariable.first) == affectedVariablesForMain->end())
		{
			// Declared variable is unused
			warnings.push_back(warning("Unused variable : " + declaredVariable.first));
		}
	}

	return 0;
}

antlrcpp::Any StaticVisitor::visitUnaryLValueExpAffectation(ifccParser::UnaryLValueExpAffectationContext *ctx)
{
	staticLog("visitUnaryLValueExpAffectation");
	auto variableName = ctx->lValue()->IDENT();
	string name = variableName->getText();

	map<string, int> &variables = declaredVariablesPerFunction[currentFunctionName];
	if (variables.find(name) != variables.end())
	{
		staticLog("Double déclaration d'une variable");
		errors.push_back(error("Redeclaration of variable " + name, variableName->getSymbol()));
		return -1;
	}

	if (ctx->lValue()->expression() != NULL)
	{
		// we have an array
		staticLog("Déclaration d'un tableau avec affectation directe");
		errors.push_back(error("Declaration of array '" + name + "' with affectation non permitted",
							   variableName->getSymbol()));
		return -1;
	}

	//Add the variable into our map of variables
	variables.insert(make_pair(name, 4));

	// The variable is being affected
	affectedVariablesPerFunction[currentFunctionName]->insert(name);

	return visitChildren(ctx);
}

antlrcpp::Any StaticVisitor::visitUnaryLValueAffectation(ifccParser::UnaryLValueAffectationContext *ctx)
{
	staticLog("visitUnaryLValueAffectation");
	auto variableName = ctx->lValue()->IDENT();

	string name = variableName->getText();

	map<string, int> &variables = declaredVariablesPerFunction[currentFunctionName];
	if (variables.find(name) != variables.end())
	{
		staticLog("Double déclaration d'une variable");
		errors.push_back(error("Redeclaration of variable " + name, variableName->getSymbol()));
		return -1;
	}

	int memory = checkDeclarationExpression(ctx->lValue());

	//Add the variable into our map of variables
	variables.insert(make_pair(name, memory));

	return visitChildren(ctx);
}

antlrcpp::Any StaticVisitor::visitDeclaration(ifccParser::DeclarationContext *ctx)
{
	staticLog("visitDeclarationStatic");
	//TODO : Gestion du type et taille allouée
	return visitChildren(ctx);
}

antlrcpp::Any StaticVisitor::visitLValue(ifccParser::LValueContext *ctx)
{
	staticLog("visitLValue");

	return visitChildren(ctx);
}

antlrcpp::Any StaticVisitor::visitAffectation(ifccParser::AffectationContext *ctx)
{
	string varName = ctx->lValue()->IDENT()->getText();

	// Log
	cerr << "visitAffectationStatic (" << varName << ")" << endl;

	map<string, int> &variables = declaredVariablesPerFunction[currentFunctionName];
	if (variables.find(varName) == variables.end())
	{
		staticLog("Affectation d'une variable non déclarée");
		errors.push_back("Affectation of undeclared variable : " + varName);
		return -1;
	}

	auto affectedVariables = affectedVariablesPerFunction[currentFunctionName];
	if (affectedVariables->find(varName) == affectedVariables->end())
	{
		// Not affected yet, add it
		affectedVariables->insert(varName);
	}

	// Exploring the expression
	visitExpression(ctx->expression());

	return 0;
}

antlrcpp::Any StaticVisitor::visitVarExpression(ifccParser::VarExpressionContext *ctx)
{
	auto var = ctx->IDENT();
	string varName = var->getText();
	cerr << "visitVarExpression, VAR (" << varName << ")" << endl;
	map<string, int> &variables = declaredVariablesPerFunction[currentFunctionName];
	if (variables.find(varName) == variables.end())
	{
		errors.push_back(error("Use of undeclared variable : " + varName, var->getSymbol()));
		return -1;
	}
	else
	{
		auto affectedVariables = affectedVariablesPerFunction[currentFunctionName];
		if (affectedVariables->find(varName) == affectedVariables->end())
		{
			warnings.push_back(warning("Use of unaffected variable : " + varName, var->getSymbol()));
		}
	}
	return 0;
}


antlrcpp::Any StaticVisitor::visitFunction(ifccParser::FunctionContext *ctx)
{
	string fctName = ctx->IDENT()->getText();

	if (find(functions.begin(), functions.end(), fctName) == functions.end())
	{
		warnings.push_back(warning("Implicit declaration of function '" + fctName + "'", ctx->IDENT()->getSymbol()));
	}

	return 0;
}

antlrcpp::Any StaticVisitor::visitExpression(ifccParser::ExpressionContext *ctx)
{
	staticLog("visit expression : " + ctx->getText());
	auto notExp = ctx->notExpression();
	if (notExp != NULL)
	{
		return visitNotExpression(notExp);
	}

	staticLog("visit expression : visit left of {" + ctx->getText() + "}");
	int left = visitExpression(ctx->expression(0)).as<int>();
	staticLog("visit expression : visit right of {" + ctx->getText() + "}");
	int right = visitExpression(ctx->expression(1)).as<int>();

	auto opVal = ctx->opr;
	string op = "&";
	if (opVal != NULL)
		op = opVal->getText();

	staticLog("visit expression : compute operande '" + op + "'. Left=" + to_string(left) + ". Right=" + to_string(right) + ".");

	if (op == "+")
		return left + right;
	if (op == "-")
		return left - right;
	if (op == "*")
		return left * right;
	if (op == "/")
	{
		if (right == 0)
			return 0;
		return left / right;
	}
	if (op == "%")
	{
		if (right == 0)
			return 0;
		return left % right;
	}
	if (op == "|")
		return left | right;
	if (op == "&")
		return left & right;
	if (op == "^")
		return left ^ right;
	if (op == "==")
		return int(left == right);
	if (op == "!=")
		return int(left != right);
	if (op == ">")
		return int(left > right);
	if (op == "<")
		return int(left < right);
	if (op == ">=")
		return int(left >= right);
	if (op == "<=")
		return int(left <= right);

	return 0;
}

antlrcpp::Any StaticVisitor::visitNotExpression(ifccParser::NotExpressionContext *ctx)
{

	staticLog("visitNotExpression : " + ctx->getText());
	int res = visitChildren(ctx).as<int>();

	staticLog("visitNotExpression {" + ctx->getText() + "} -> inside=" + to_string(res));
	if (ctx->getText().front() == '!')
	{
		return int(!res);
	}
	return res;
}


antlrcpp::Any StaticVisitor::visitFonctionExpression (ifccParser::FonctionExpressionContext *ctx)
{
	staticLog("visitFonctionExpression ");
	return 0;
}


antlrcpp::Any StaticVisitor::visitConstExpression(ifccParser::ConstExpressionContext *ctx)
{
	staticLog("visitConstExpression : " + ctx->getText());
	return stoi(ctx->getText());
}

antlrcpp::Any StaticVisitor::visitParExpression(ifccParser::ParExpressionContext *ctx)
{
	return visitExpression(ctx->expression());
}

antlrcpp::Any StaticVisitor::visitArrayExpression(ifccParser::ArrayExpressionContext *ctx)
{
	auto var = ctx->IDENT();
	string varName = var->getText();
	cerr << "visitVarExpression, VAR (" << varName << ")" << endl;
	map<string, int> &variables = declaredVariablesPerFunction[currentFunctionName];
	if (variables.find(varName) == variables.end())
	{
		errors.push_back(error("Use of undeclared variable : " + varName, var->getSymbol()));
		return -1;
	}
	else
	{
		auto affectedVariables = affectedVariablesPerFunction[currentFunctionName];
		if (affectedVariables->find(varName) == affectedVariables->end())
		{
			warnings.push_back(warning("Use of unaffected variable : " + varName, var->getSymbol()));
		}
	}
	return 0;
}


bool StaticVisitor::printErrors()
{
	for (string s : errors)
	{
		cerr << s << endl;
	}
	return !errors.empty();
}

void StaticVisitor::printWarnings()
{
	for (string s : warnings)
		cerr << s << endl;
}

map<string, int> StaticVisitor::memoryNeededPerFunction()
{
	map<string, int> result;
	for (const auto &p : declaredVariablesPerFunction)
	{
		int memoryNeeded = 0;
		for (const auto &t : p.second)
		{
			memoryNeeded += t.second;
		}
		result.insert(make_pair(p.first, memoryNeeded));
	}
	return result;
}

StaticVisitor::~StaticVisitor()
{
	for (auto it = affectedVariablesPerFunction.begin(); it != affectedVariablesPerFunction.end(); ++it)
	{
		delete it->second;
	}
}
