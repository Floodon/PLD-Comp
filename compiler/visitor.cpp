// Copied from a file generated from ifcc.g4 by ANTLR 4.7.2

#include "visitor.h"

#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <typeinfo>
#include "expressionResult.h"
using namespace std;

// ==== Constants

const string REGISTER_RESULT = "eax";
const string REGISTER_1 = "edx";
const string REGISTER_2 = "esp";
const string REGISTER_3 = "ebx";
const string REGISTER_FCT = "edi";
const string REGISTER_COND = "al";

const string COMPARE_SUFFIXES[] = {"e", "g", "ge", "l", "le", "ne"};
const int EQUALS = 0, GREATER = 1, GREATER_OR_EQUAL = 2, LIGHTER = 3, LIGHTER_OR_EQUAL = 4, NON_EQUAL = 5;

const string BIT_WISE_PREFIXES[] = {"and", "or", "xor"};
const int AND = 0, OR = 1, XOR = 2;

// ==== Functions

// Useful

/**
 * Write a message into the Logcat (cerr here)
 */
void log(string message)
{
	cerr << "Visitor: " << message << endl;
}

/**
 * Place the constant into the addr.
 * [addr] must be negative and a multiple of -4
 */
void moveConstToAddr(int constant, int addr)
{
	cout << "	movl	$" << constant << ", " << addr << "(%rbp)\n";
}

/**
 * Place the constant into a register
 */
void moveConstToRegister(int constant, string reg)
{
	cout << "	movl	$" << constant << ", %" << reg << "\n";
}

/**
 * Move the content of the register [regFrom] into the register [regTo]
 */
void moveRegisterToRegister(string regFrom, string regTo)
{
	cout << "	movl	%" << regFrom << ", %" << regTo << "\n";
}

/**
 * Move the content of the register [reg] into [addr].
 * [addr] must be multiple of -4
 */
void moveRegisterToAddr(string reg, int addr)
{
	cout << "	movl	%" << reg << ", " << addr << "(%rbp)\n";
}

/**
 * Move the content of [addr] into the register [reg].
 * [addr] must be multiple of -4
 */
void moveAddrToRegister(int addr, string reg)
{
	cout << "	movl	" << addr << "(%rbp), %" << reg << "\n";
}

void moveAddrToAddr(int addr1, int addr2)
{
	moveAddrToRegister(addr1, REGISTER_1);
	moveRegisterToAddr(REGISTER_1, addr2);
}

/**
 * Move the result of the expression in [toMove] into the array corresponding to the address [arrayAddr]
 * at the array index stored into [arrayIndex]
 */
void moveExpressionToArray(ExpressionResult *toMove, ExpressionResult *arrayIndex, int arrayAddr)
{
	switch (arrayIndex->getType())
	{
	case TYPE_CONST:
		switch (toMove->getType())
		{
		case TYPE_CONST:
			// array[const] = const
			moveConstToAddr(toMove->getValue(), arrayAddr + 4 * arrayIndex->getValue());
			break;

		case TYPE_VARIABLE:
			// array[const] = var
			moveAddrToAddr(toMove->getValue(), arrayAddr + 4 * arrayIndex->getValue());
			break;

		case TYPE_REGISTER:
			// array[const] = reg
			moveRegisterToAddr(REGISTER_RESULT, arrayAddr + 4 * arrayIndex->getValue());
		}
		break;
	case TYPE_VARIABLE:
		switch (toMove->getType())
		{
		case TYPE_CONST:
			// array[variable] = const
			moveAddrToRegister(arrayIndex->getValue(), REGISTER_RESULT);
			cout << "	cltq\n";
			cout << "	movl	$" << toMove->getValue()
				 << ", " << arrayAddr << "(%rbp,%rax,4)\n";
			break;
		case TYPE_VARIABLE:
			// array[variable] = variable
			moveAddrToRegister(arrayIndex->getValue(), REGISTER_RESULT);
			cout << "	cltq\n";
			cout << "	movl	" << toMove->getValue() << "(%rbp)"
				 << ", " << arrayAddr << "(%rbp,%rax,4)\n";
			break;
		case TYPE_REGISTER:
			// array[variable] = register
			moveRegisterToRegister(REGISTER_RESULT, REGISTER_2);
			moveAddrToRegister(arrayIndex->getValue(), REGISTER_RESULT);
			cout << "	cltq\n";
			cout << "	movl	%" << REGISTER_2
				 << ", " << arrayAddr << "(%rbp,%rax,4)\n";
			break;
		}
		break;
		// Impossible for array index to be register
	}
}

ExpressionResult *moveArrayToRegister(ExpressionResult *arrayIndex, int arrayAddr)
{
	switch (arrayIndex->getType())
	{
	case TYPE_CONST:
		// array[const] --> return the variable index
		return new ExpressionResult(TYPE_VARIABLE, arrayAddr + 4 * arrayIndex->getValue());
	case TYPE_VARIABLE:
		// array[var] --> move var to reg and act as if is was array[reg]
		moveAddrToRegister(arrayIndex->getValue(), REGISTER_RESULT);
	case TYPE_REGISTER:
		// array[reg]
		cout << "	cltq\n";
		cout << "	movl	" << arrayAddr << "(%rbp,%rax,4), %eax\n";
		return new ExpressionResult(TYPE_REGISTER, 0);
	}
	return NULL;
}

// === Inherited Functions ===
antlrcpp::Any Visitor::visitProg(ifccParser::ProgContext *ctx)
{
	log("visitProg");
	cerr << ctx->toStringTree() << endl;

	return visitChildren(ctx);
}

antlrcpp::Any Visitor::visitMain(ifccParser::MainContext *ctx)
{
	// Find memory needed (use only multiple of 16)
	int memoryNeeded = (memoryNeededPerFunction["main"] / 16 + 1) * 16;

	log("visitProg : memoryNeeded=" + to_string(memoryNeeded));

	// Program begin
	std::cout << ".globl	main\n"
				 " main: \n"
				 "	pushq	%rbp\n"
				 "	movq	%rsp, %rbp\n"
				 "	subq	$"
			  << memoryNeeded << ", %rsp\n";

	return visitChildren(ctx);
}

antlrcpp::Any Visitor::visitCondition(ifccParser::ConditionContext *ctx)
{
	if (ctx->ELSE())
	{
		int firstJump = nbIf++;
		int secondJump = nbIf++;
		log("visitConditionElse");
		visitExpression(ctx->expression());
		cout << "	cmpl	$0, %" << REGISTER_RESULT << "\n";
		std::cout << "	je .L" << firstJump << endl;
		visit(ctx->instruction(0));
		std::cout << "	jmp .L" << secondJump << endl;
		std::cout << ".L" << firstJump << ":" << endl;
		visit(ctx->instruction(1));
		std::cout << ".L" << secondJump << ":" << endl;
	}
	else
	{
		log("visitCondition");
		ExpressionResult *result = visitExpression(ctx->expression()).as<ExpressionResult *>();
		switch (result->getType())
		{
		case TYPE_CONST:
			if (result->getValue() != 0)
			{
				visit(ctx->instruction(0));
			}
			break;

		case TYPE_VARIABLE:
			cout << "	cmpl	$0, " << result->getValue() << "(%rbp)\n";
			std::cout << "	je .L" << nbIf << endl;
			visit(ctx->instruction(0));
			std::cout << ".L" << nbIf << ":" << endl;
			break;

		case TYPE_REGISTER:
			cout << "	cmpl	$0, %" << REGISTER_RESULT << "\n";
			std::cout << "	je .L" << nbIf << endl;
			visit(ctx->instruction(0));
			std::cout << ".L" << nbIf << ":" << endl;
			break;
		}
		nbIf++;
		delete result;
	}
	return 0;
}

antlrcpp::Any Visitor::visitLoopWhile(ifccParser::LoopWhileContext *ctx)
{
	log("visitLoopWhile");
	string condLabel = ".L" + to_string(nbIf);
	string loopLabel = ".L" + to_string(nbIf + 1);
	nbIf += 2;

	cout << "	jmp	" << condLabel << "\n";

	cout << loopLabel << ":\n";
	visitBlock(ctx->block());

	cout << condLabel << ":\n";
	ExpressionResult *result = visitExpression(ctx->expression()).as<ExpressionResult *>();

	switch (result->getType())
	{
	case TYPE_CONST:
		if (0 != result->getValue())
		{
			// Inifinite while
			log("visitLoopWhile : WARNING infinite while found ");
			cout << "	jmp	" << loopLabel << "\n";
		}
		else
		{
			log("visitLoopWhile : WARNING useless while found ");
		}
		break;
	case TYPE_VARIABLE:
		cout << "	cmpl	$0, " << result->getValue() << "(%rbp)\n";
		std::cout << "	jne	" << loopLabel << "\n";
		break;
	case TYPE_REGISTER:
		cout << "	cmpl	$0, %" << REGISTER_RESULT << "\n";
		std::cout << "	jne	" << loopLabel << "\n";
		break;
	}

	delete result;

	return 0;
}

antlrcpp::Any Visitor::visitLoopFor(ifccParser::LoopForContext *ctx)
{
	log("visitLoopfor");
	string condLabel = ".L" + to_string(nbIf);
	string loopLabel = ".L" + to_string(nbIf + 1);
	nbIf += 2;

	// 1. Visit init
	auto forStart = ctx->forStart();
	if (forStart != NULL)
	{
		visitForStart(forStart);
	}

	// 2. Jump to condition
	cout << "	jmp	" << condLabel << "\n";

	// 3. Visit for content
	cout << loopLabel << ":\n";

	//	3.1. Block
	visitBlock(ctx->block());

	//	3.2. For end
	auto forEnd = ctx->forEnd();
	if (forEnd != NULL)
	{
		visitForEnd(forEnd);
	}

	// 	4. Visit condition
	cout << condLabel << ":\n";

	auto expression = ctx->expression();
	if (expression == NULL)
	{
		log("visitLoopFor : WARNING infinite for found ");
		cout << "	jmp	" << loopLabel << "\n";
	}
	else
	{
		ExpressionResult *result = visitExpression(expression).as<ExpressionResult *>();

		switch (result->getType())
		{
		case TYPE_CONST:
			if (0 != result->getValue())
			{
				// Inifinite while
				log("visitLoopWhile : WARNING infinite while found ");
				cout << "	jmp	" << loopLabel << "\n";
			}
			else
			{
				log("visitLoopWhile : WARNING useless while found ");
			}
			break;
		case TYPE_VARIABLE:
			cout << "	cmpl	$0, " << result->getValue() << "(%rbp)\n";
			std::cout << "	jne	" << loopLabel << "\n";
			break;
		case TYPE_REGISTER:
			cout << "	cmpl	$0, %" << REGISTER_RESULT << "\n";
			std::cout << "	jne	" << loopLabel << "\n";
			break;
		}

		delete result;
	}

	return 0;
}

antlrcpp::Any Visitor::visitFunction(ifccParser::FunctionContext *ctx)
{
	log("visitFunction");

	int constVal = stoi(ctx->CONST()->getText());
	string fctName = ctx->IDENT()->getText();

	moveConstToRegister(constVal, REGISTER_FCT);

	std::cout << "	call " << fctName << "@PLT" << endl;
	return 0;
}

antlrcpp::Any Visitor::visitUnaryLValueAffectation(ifccParser::UnaryLValueAffectationContext *ctx)
{
	log("visitUnaryLValueAffectation");

	auto expression = ctx->lValue()->expression();
	if (expression == NULL)
	{
		// Pas array
		log("visitUnaryLValueAffectation: VarLValueContext");
		string varName = ctx->lValue()->IDENT()->getText();

		//Add the variable into our map of variables
		int stackPointer = -(++varCount) * 4;
		variables.insert(make_pair(varName, stackPointer));
		cerr << "add " << varName << " to the variables at pos " << stackPointer << endl;
	}
	else
	{
		// Array
		log("visitUnaryLValueAffectation: ArrayLValueContext");
		string varName = ctx->lValue()->IDENT()->getText();

		// Visit the expression (size of array)
		ExpressionResult *expressionResult = visitExpression(expression).as<ExpressionResult *>();

		log("visitUnaryLValueAffectation: the type of the expression result is : " + to_string(expressionResult->getType()));

		// Add array name into var map
		int arraySize = expressionResult->getValue(); //Must be a const
		varCount = varCount + 1 + arraySize;
		int stackPointer = -(varCount)*4; //Need to be at the lower index
		variables.insert(make_pair(varName, stackPointer));
		cerr << "add " << varName << " to the variables at pos " << stackPointer << endl;
		cerr << arraySize << " reservés" << endl;

		delete expressionResult;
	}

	return 0;
}

antlrcpp::Any Visitor::visitUnaryLValueExpAffectation(ifccParser::UnaryLValueExpAffectationContext *ctx)
{
	log("visitUnaryLValueExpAffectation");
	auto variableName = ctx->lValue();

	string name = variableName->getText();

	//Add the variable into our map of variables
	int stackPointer = -(++varCount) * 4;
	variables.insert(make_pair(name, stackPointer));

	auto expression = ctx->expression();

	ExpressionResult *result = visitExpression(ctx->expression()).as<ExpressionResult *>();

	switch (result->getType())
	{
	case TYPE_CONST:
		moveConstToAddr(result->getValue(), stackPointer);
		break;
	case TYPE_VARIABLE:
		moveAddrToAddr(result->getValue(), stackPointer);
		break;
	case TYPE_REGISTER:
		moveRegisterToAddr(REGISTER_RESULT, stackPointer);
	}

	delete result;
	return 0;
}

antlrcpp::Any Visitor::visitDeclaration(ifccParser::DeclarationContext *ctx)
{
	log("visitDeclaration");
	//TODO : Gestion du type et taille allouée
	visitChildren(ctx);
	return 0;
}

antlrcpp::Any Visitor::visitDeclarationFonction(ifccParser::DeclarationFonctionContext *ctx)
{
	log("visitDeclarationFonction");
	//TODO : Gestion du type et taille allouée
	int memoryNeeded = (memoryNeededPerFunction[ctx->getText()]/16 + 1)*16;

	log("visit "+ctx->getText() +": memoryNeeded="+to_string(memoryNeeded));

	cout <<
			ctx->IDENT()->getText() + ":\n"
			"	pushq	%rbp\n"
			"	movq	%rsp, %rbp\n"
			"	subq	$"<<memoryNeeded<<", %rsp\n";
	visitChildren(ctx);
	cout << endl;
	return 0;
}

antlrcpp::Any Visitor::visitAppelFonction(ifccParser::AppelFonctionContext *ctx)
{
	log("visitAppelFonction");
	// Program begin
	std::cout << 
				 " 	call	"+ ctx->IDENT()->getText();
	
	log(ctx->getText());
	visitChildren(ctx);
	cout << endl;
	return 0;
}

antlrcpp::Any Visitor::visitFonctionExpression(ifccParser::FonctionExpressionContext *ctx)
{
	log("visitFonctionExpression");
	visitChildren(ctx);
    return new ExpressionResult(TYPE_REGISTER, 0);

}

antlrcpp::Any Visitor::visitAffectation(ifccParser::AffectationContext *ctx)
{
	string varName = ctx->lValue()->IDENT()->getText();

	// Log
	cerr << "visitAffectation (" << varName << ")" << endl;

	// Find the memory addresse of the variable
	map<string, int>::iterator it = variables.find(varName);

	auto arrayExpression = ctx->lValue()->expression();
	int arrayAddr = it->second;
	ExpressionResult *resultIntoBrackets = NULL;

	// We may store the result of our left expression into a temporary variable
	string temporaryName;
	int tempSP;
	bool temporaryVariableCreated = false;

	if (arrayExpression != NULL)
	{
		// Array : Explore the expression into brackets
		resultIntoBrackets = visitExpression(arrayExpression).as<ExpressionResult *>();

		if (resultIntoBrackets->getType() == TYPE_REGISTER)
		{
			temporaryName = "!tmp" + to_string(++tempCount);
			tempSP = -(++varCount) * 4;
			variables.insert(make_pair(temporaryName, tempSP));
			cerr << "visitAffectation, create temp var '" << temporaryName << "' in addr " << tempSP << endl;

			// Save the result from eax to temp
			moveRegisterToAddr(REGISTER_RESULT, tempSP);

			resultIntoBrackets->setType(TYPE_VARIABLE);
			resultIntoBrackets->setValue(tempSP);

			temporaryVariableCreated = true;
		}
	}

	// Exploring the expression
	ExpressionResult *result = visitExpression(ctx->expression()).as<ExpressionResult *>();

	if (arrayExpression != NULL)
	{
		// We are affecting an array
		moveExpressionToArray(result, resultIntoBrackets, arrayAddr);

		// Free temporary variable if needed
		if (temporaryVariableCreated)
		{
			cerr << "visitAffectation, free temp var '" << temporaryName << "' in addr " << tempSP << endl;
			variables.erase(temporaryName);
			tempCount--;
			varCount--;
		}

		delete resultIntoBrackets;
	}
	else
	{
		// Affect a single variable
		switch (result->getType())
		{
		case TYPE_CONST:
			moveConstToAddr(result->getValue(), arrayAddr);
			break;
		case TYPE_VARIABLE:
			moveAddrToAddr(result->getValue(), arrayAddr);
			break;
		case TYPE_REGISTER:
			moveRegisterToAddr(REGISTER_RESULT, arrayAddr);
		}
	}

	return 0;
}

antlrcpp::Any Visitor::visitRet(ifccParser::RetContext *ctx)
{
	log("visitRet");

	// Exploring the expression
	ExpressionResult *result = visitExpression(ctx->expression()).as<ExpressionResult *>();

	switch (result->getType())
	{
	case TYPE_CONST:
		moveConstToRegister(result->getValue(), REGISTER_RESULT);
		break;
	case TYPE_VARIABLE:
		moveAddrToRegister(result->getValue(), REGISTER_RESULT);
		break;
	}

	cout
		<< "	leave\n"
		   "	ret\n";

	delete result;

	return 0;
}

antlrcpp::Any Visitor::visitParExpression(ifccParser::ParExpressionContext *ctx)
{
	cerr << "visitParExpression (" << ctx->getText() << ")" << endl;

	return visitExpression(ctx->expression()); // Visit expression
}

antlrcpp::Any Visitor::visitConstExpression(ifccParser::ConstExpressionContext *ctx)
{
	cerr << "visitConstExpression (" << ctx->getText() << ")" << endl;

	int constValue = stoi(ctx->CONST()->getText());
	//moveConstToRegister(constValue, REGISTER_RESULT);
	return new ExpressionResult(TYPE_CONST, constValue);
}

antlrcpp::Any Visitor::visitVarExpression(ifccParser::VarExpressionContext *ctx)
{
	cerr << "visitVarExpression (" << ctx->getText() << ")" << endl;

	string varName = ctx->IDENT()->getText();
	map<string, int>::iterator it = variables.find(varName);
	if (it != variables.end())
	{
		return new ExpressionResult(TYPE_VARIABLE, it->second);
	}

	return 0;
}

antlrcpp::Any Visitor::visitArrayExpression(ifccParser::ArrayExpressionContext *ctx)
{
	log("visitArrayExpression");

	string varName = ctx->IDENT()->getText();
	map<string, int>::iterator it = variables.find(varName);
	if (it != variables.end())
	{
		log("visitArrayExpression -> visit expression into bracet");
		ExpressionResult *index = visitExpression(ctx->expression()).as<ExpressionResult *>();
		int arrayAddr = it->second;
		auto result = moveArrayToRegister(index, arrayAddr);
		delete index;
		return result;
	}
	return 0;
}

antlrcpp::Any Visitor::visitLValue(ifccParser::LValueContext *ctx)
{
	log("visitArrayLValue");
	log("TODO visitArrayLValue not implemented");
	return 0;
}

/**
 * Return an expression result 
 * If the below result is a const, perform the operation in the compiler
 * If it is a variable, add lines and store result in eax
 * Else, add lines, and keep result in eax
 * If there are no '!', send the result of below expression
 */
antlrcpp::Any Visitor::visitNotExpression(ifccParser::NotExpressionContext *ctx)
{
	log("visitNotExpression");
	ExpressionResult *result = visitChildren(ctx).as<ExpressionResult *>();

	log("visitNotExpression (" + ctx->getText() + ") result=" + result->toString());

	if (ctx->getText().front() == '!')
	{
		switch (result->getType())
		{
		case TYPE_CONST:
			// Constant : change is content
			result->setValue(!(result->getValue()));
			return result;

		case TYPE_VARIABLE:
			cout << "	cmpl	$0, " << result->getValue() << "(%rbp)\n";
			cout << "	sete	%al\n";
			cout << "	movzbl	%al, %" << REGISTER_RESULT << "\n";
			delete result;
			return new ExpressionResult(TYPE_REGISTER, 0);

		case TYPE_REGISTER:
			cout << "	cmpl	$0, %" << REGISTER_RESULT << "\n";
			cout << "	sete	%al\n";
			cout << "	movzbl	%al, %" << REGISTER_RESULT << "\n";
			return result;
		}
	}

	return result;
}

ExpressionResult *addResults(ExpressionResult *left, ExpressionResult *right)
{
	log("addResults " + left->toString() + ", " + right->toString());

	if (left->getType() == TYPE_REGISTER)
	{
		log("addResults : ERROR ! left is in a register, this must be impossible.");
		return NULL;
	}

	switch (left->getType())
	{
	case TYPE_CONST:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Const + Const --> Pre process it
			return new ExpressionResult(TYPE_CONST, left->getValue() + right->getValue());
		case TYPE_VARIABLE:
			// Const + Var --> var->reg, const+reg
			moveAddrToRegister(right->getValue(), REGISTER_RESULT);
			cout << "	addl	$" << left->getValue() << ", %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Const + reg --> Const+reg->reg
			cout << "	addl	$" << left->getValue() << ", %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		break;
	case TYPE_VARIABLE:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Var + Const --> var->reg, const+reg
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	addl	$" << right->getValue() << ", %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_VARIABLE:
			// Var + Var --> var->reg, var+reg
			moveAddrToRegister(right->getValue(), REGISTER_RESULT);
			cout << "	addl	" << left->getValue() << "(%rbp), %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Var + Reg --> var+reg->reg
			cout << "	addl	" << left->getValue() << "(%rbp), %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		// Can not be a register
	}
	log("Error add results : return null");
	return NULL;
}

ExpressionResult *substractResults(ExpressionResult *left, ExpressionResult *right)
{
	log("substractResults " + left->toString() + " - " + right->toString());

	if (left->getType() == TYPE_REGISTER)
	{
		log("substractResults : ERROR ! left is in a register, this must be impossible.");
		return NULL;
	}

	switch (left->getType())
	{
	case TYPE_CONST:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Const + Const --> Pre process it
			return new ExpressionResult(TYPE_CONST, left->getValue() - right->getValue());
		case TYPE_VARIABLE:
			// Const - Var --> Var->reg, do const - reg
			moveAddrToRegister(right->getValue(), REGISTER_RESULT);
			//cout << "	subl	" << right->getValue() << "(%rbp), %" << REGISTER_RESULT << "\n";
			//return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Const - reg --> Sub reg, const
			// We do -(reg - const) --> negl (sub const reg)
			cout << "	subl	$" << left->getValue() << ", %" << REGISTER_RESULT << "\n";
			cout << "	negl	%" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		break;
	case TYPE_VARIABLE:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Var - Const --> sub const var / var->reg, sub const reg
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	subl	$" << right->getValue() << ", %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_VARIABLE:
			// V1 - V2 --> sub v2 v1 --> v1->reg / sub v2 reg
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	subl	" << right->getValue() << "(%rbp), %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Var - Reg --> sub reg var
			// =-(reg - var) --> negl (sub var reg)
			cout << "	subl	" << left->getValue() << "(%rbp), %" << REGISTER_RESULT << "\n";
			cout << "	negl	%" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		// Can not be a register
	}
	return NULL;
}

ExpressionResult *multiplyResults(ExpressionResult *left, ExpressionResult *right)
{
	log("multiplyResults " + left->toString() + " - " + right->toString());

	if (left->getType() == TYPE_REGISTER)
	{
		log("multiplyResults : ERROR ! left is in a register, this must be impossible.");
		return NULL;
	}

	/*

		// Multiply right with left, and store the result into REG_RES
		cout << "	imull	" << tempSP << "(%rbp), %" << REGISTER_RESULT << "\n";
	*/

	switch (left->getType())
	{
	case TYPE_CONST:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Const * Const --> Pre process it
			return new ExpressionResult(TYPE_CONST, left->getValue() * right->getValue());
		case TYPE_VARIABLE:
			// Const * Var --> var->reg, const+reg
			moveConstToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	imull	" << right->getValue() << "(%rbp), %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Const * reg --> mull const, reg
			cout << "	imull	$" << left->getValue() << ", %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		break;
	case TYPE_VARIABLE:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Var * Const --> var->reg, mull const reg
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	imull	$" << right->getValue() << ", %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_VARIABLE:
			// V1 * V2 --> v1->reg / v2 * reg
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	imull	" << right->getValue() << "(%rbp), %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Var * Reg --> mull var reg
			cout << "	imull	" << left->getValue() << "(%rbp), %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		// Can not be a register
	}
	return NULL;
}

ExpressionResult *divideResults(ExpressionResult *left, ExpressionResult *right)
{
	log("divideResults " + left->toString() + " - " + right->toString());

	if (left->getType() == TYPE_REGISTER)
	{
		log("divideResults : ERROR ! left is in a register, this must be impossible.");
		return NULL;
	}

	/*
	regRes = numerator (left)
	reg1 clean

			 Store right in another reg
		moveRegisterToRegister(REGISTER_RESULT, REGISTER_1);
			 Move left into REG_RES (used by idivl as numerator)
		moveAddrToRegister(tempSP, REGISTER_RESULT);
			 Move right into our temp variable because REG_1 is used by idivl (cltd need to used REG_RES + REG_1)
		moveRegisterToAddr(REGISTER_1, tempSP);
		cout << "	cltd\n";
		cout << "	idivl	" << tempSP << "(%rbp)\n";
			 The result is in RES
*/

	switch (left->getType())
	{
	case TYPE_CONST:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Const / Const --> Pre process it
			if (right->getValue() == 0)
			{
				log("Warning, dividing by 0");
			}
			return new ExpressionResult(TYPE_CONST, left->getValue() / right->getValue());
		case TYPE_VARIABLE:
			// Const / Var --> var->reg, reg/const
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	cltd\n";
			cout << "	idivl	" << right->getValue() << "(%rbp)\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Const / reg --> reg->reg2, const->reg --> reg/reg2
			moveRegisterToRegister(REGISTER_RESULT, REGISTER_2);
			moveConstToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	cltd\n";
			cout << "	idivl	%" << REGISTER_2 << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		break;
	case TYPE_VARIABLE:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Var / Const --> var->reg, reg/const
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			moveConstToRegister(right->getValue(), REGISTER_2);
			cout << "	cltd\n";
			cout << "	idivl	%" << REGISTER_2 << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_VARIABLE:
			// V1 / V2 --> v1->reg --> reg/v2
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	cltd\n";
			cout << "	idivl	" << right->getValue() << "(%rbp)\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Var / Reg --> reg->reg2, var->reg --> reg/reg2
			moveRegisterToRegister(REGISTER_RESULT, REGISTER_2);
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	cltd\n";
			cout << "	idivl	%" << REGISTER_2 << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		// Can not be a register
	}
	return NULL;
}

ExpressionResult *moduloResults(ExpressionResult *left, ExpressionResult *right)
{
	log("divideResults " + left->toString() + " - " + right->toString());

	if (left->getType() == TYPE_REGISTER)
	{
		log("divideResults : ERROR ! left is in a register, this must be impossible.");
		return NULL;
	}

	/*
	numerator (left) into eax
	denominator (right) into reg or var
	result in edx, move it to eax
			 Store right in another reg
		moveRegisterToRegister(REGISTER_RESULT, REGISTER_1);
			 Move left into REG_RES (used by idivl as numerator)
		moveAddrToRegister(tempSP, REGISTER_RESULT);
			 Move right into our temp variable because REG_1 is used by idivl (cltd need to used REG_RES + REG_1)
		moveRegisterToAddr(REGISTER_1, tempSP);
		cout << "	cltd\n";
		cout << "	idivl	" << tempSP << "(%rbp)\n";
			 The result is in 1 (edx)
		moveRegisterToRegister(REGISTER_1, REGISTER_RESULT);
*/

	switch (left->getType())
	{
	case TYPE_CONST:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Const % Const --> Pre process it
			return new ExpressionResult(TYPE_CONST, left->getValue() % right->getValue());
		case TYPE_VARIABLE:
			// Const % Var --> var->reg, reg/const
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	cltd\n";
			cout << "	idivl	" << right->getValue() << "(%rbp)\n";
			moveRegisterToRegister(REGISTER_1, REGISTER_RESULT);
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Const % reg --> reg->reg2, const->reg --> reg/reg2
			moveRegisterToRegister(REGISTER_RESULT, REGISTER_2);
			moveConstToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	cltd\n";
			cout << "	idivl	%" << REGISTER_2 << "\n";
			moveRegisterToRegister(REGISTER_1, REGISTER_RESULT);
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		break;
	case TYPE_VARIABLE:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Var % Const --> var->reg, reg/const
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			moveConstToRegister(right->getValue(), REGISTER_2);
			cout << "	cltd\n";
			cout << "	idivl	%" << REGISTER_2 << "\n";
			moveRegisterToRegister(REGISTER_1, REGISTER_RESULT);
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_VARIABLE:
			// V1 % V2 --> v1->reg --> reg/v2
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	cltd\n";
			cout << "	idivl	" << right->getValue() << "(%rbp)\n";
			moveRegisterToRegister(REGISTER_1, REGISTER_RESULT);
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Var % Reg --> reg->reg2, var->reg --> reg/reg2
			moveRegisterToRegister(REGISTER_RESULT, REGISTER_2);
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	cltd\n";
			cout << "	idivl	%" << REGISTER_2 << "\n";
			moveRegisterToRegister(REGISTER_1, REGISTER_RESULT);
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		// Can not be a register
	}
	return NULL;
}

ExpressionResult *compareResults(ExpressionResult *left, ExpressionResult *right, int compareType)
{
	log("compareResults " + left->toString() + " - " + right->toString());

	if (left->getType() == TYPE_REGISTER)
	{
		log("compareResults : ERROR ! left is in a register, this must be impossible.");
		return NULL;
	}

	/*
		cout << "	cmpl 	" << tempSP << "(%rbp), %" << REGISTER_RESULT << "\n";
		cout << "	sete	%al\n";
		cout << "	movzbl	%al, %" << REGISTER_RESULT << "\n";
	*/

	switch (left->getType())
	{
	case TYPE_CONST:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Const == Const --> Pre process it
			switch (compareType)
			{
			case EQUALS:
				return new ExpressionResult(TYPE_CONST, left->getValue() == right->getValue());
			case GREATER:
				return new ExpressionResult(TYPE_CONST, left->getValue() > right->getValue());
			case GREATER_OR_EQUAL:
				return new ExpressionResult(TYPE_CONST, left->getValue() >= right->getValue());
			case LIGHTER:
				return new ExpressionResult(TYPE_CONST, left->getValue() < right->getValue());
			case LIGHTER_OR_EQUAL:
				return new ExpressionResult(TYPE_CONST, left->getValue() <= right->getValue());
			case NON_EQUAL:
				return new ExpressionResult(TYPE_CONST, left->getValue() != right->getValue());
			}
			break;
		case TYPE_VARIABLE:
			// Const comp Var --> var->reg, const comp reg
			moveAddrToRegister(right->getValue(), REGISTER_RESULT);
			cout << "	cmpl 	$" << left->getValue() << ", %" << REGISTER_RESULT << "\n";
			cout << "	set" + COMPARE_SUFFIXES[compareType] + "	%al\n";
			cout << "	movzbl	%al, %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Const == reg -->
			cout << "	cmpl 	$" << left->getValue() << ", %" << REGISTER_RESULT << "\n";
			cout << "	set" + COMPARE_SUFFIXES[compareType] + "	%al\n";
			cout << "	movzbl	%al, %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		break;
	case TYPE_VARIABLE:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Var == Const -->  const==var
			cout << "	cmpl 	$" << right->getValue() << ", " << left->getValue() << "(%rbp)\n";
			cout << "	set" + COMPARE_SUFFIXES[compareType] + "	%" << REGISTER_COND << "\n";
			cout << "	movzbl	%" << REGISTER_COND << ", %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_VARIABLE:
			// V1 == V2 --> v1->reg / v2==reg
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	cmpl 	" << right->getValue() << "(%rbp), %" << REGISTER_RESULT << "\n";
			cout << "	set" + COMPARE_SUFFIXES[compareType] + "	%al\n";
			cout << "	movzbl	%al, %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Var == Reg -->
			cout << "	cmpl 	%" << REGISTER_RESULT << ", " << left->getValue() << "(%rbp)\n";
			cout << "	set" + COMPARE_SUFFIXES[compareType] + "	%al\n";
			cout << "	movzbl	%al, %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		// Can not be a register
	}
	return NULL;
}

ExpressionResult *bitWiseOperation(ExpressionResult *left, ExpressionResult *right, int operation)
{
	log("bitWiseOperation " + left->toString() + " - " + right->toString());

	if (left->getType() == TYPE_REGISTER)
	{
		log("bitWiseOperation : ERROR ! left is in a register, this must be impossible.");
		return NULL;
	}

	/*
		op	v1	v2(res)
			 Store right in another reg
		moveRegisterToRegister(REGISTER_RESULT, REGISTER_1);
			 Move left into REG_RES (used by idivl as numerator)
		moveAddrToRegister(tempSP, REGISTER_RESULT);
			 Move right into our temp variable because REG_1 is used by idivl (cltd need to used REG_RES + REG_1)
		moveRegisterToAddr(REGISTER_1, tempSP);
			 [left]|[right] --> orl [right] [left], result in [left] (here eax)
		cout << "	orl " << tempSP << "(%rbp), %" << REGISTER_RESULT << "\n";
	*/

	switch (left->getType())
	{
	case TYPE_CONST:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Const == Const --> Pre process it
			switch (operation)
			{
			case AND:
				return new ExpressionResult(TYPE_CONST, left->getValue() & right->getValue());
			case OR:
				return new ExpressionResult(TYPE_CONST, left->getValue() | right->getValue());
			case XOR:
				return new ExpressionResult(TYPE_CONST, left->getValue() ^ right->getValue());
			}
			break;
		case TYPE_VARIABLE:
			// Const op Var --> var->reg, const op reg
			moveAddrToRegister(right->getValue(), REGISTER_RESULT);
			cout << "	" << BIT_WISE_PREFIXES[operation]
				 << "l 	$" << left->getValue() << ", %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Const op reg -->
			cout << "	" << BIT_WISE_PREFIXES[operation]
				 << "l 	$" << left->getValue() << ", %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		break;
	case TYPE_VARIABLE:
		switch (right->getType())
		{
		case TYPE_CONST:
			// Var op Const -->  var->reg, const op reg
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	" << BIT_WISE_PREFIXES[operation]
				 << "l 	$" << right->getValue() << ", %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_VARIABLE:
			// V1 op V2 --> v1->reg / v2 op reg
			moveAddrToRegister(left->getValue(), REGISTER_RESULT);
			cout << "	" << BIT_WISE_PREFIXES[operation]
				 << "l 	" << right->getValue() << "(%rbp), %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		case TYPE_REGISTER:
			// Var op Reg --> var op reg
			cout << "	" << BIT_WISE_PREFIXES[operation]
				 << "l 	" << left->getValue() << "(%rbp), %" << REGISTER_RESULT << "\n";
			return new ExpressionResult(TYPE_REGISTER, 0);
		}
		// Can not be a register
	}
	return NULL;
}

antlrcpp::Any Visitor::visitExpression(ifccParser::ExpressionContext *ctx)
{
	cerr << "visitExpression (" << ctx->getText() << ")" << endl;

	auto unaryExpression = ctx->notExpression();
	if (unaryExpression != NULL)
	{
		// A or -A
		ExpressionResult *result = visitChildren(ctx).as<ExpressionResult *>(); // Visit unaryExpression

		char startChar = ctx->getText().front();
		if (startChar == '-')
		{
			switch (result->getType())
			{
			case TYPE_CONST:
				result->setValue(-result->getValue());
				return result;
			case TYPE_VARIABLE:
				moveAddrToRegister(result->getValue(), REGISTER_RESULT);
				delete result;
				cout << "	negl	%" << REGISTER_RESULT << "\n";
				return new ExpressionResult(TYPE_REGISTER, 0);
			case TYPE_REGISTER:
				cout << "	negl	%" << REGISTER_RESULT << "\n";
				return result;
			}
		}
		return result;
	}

	// We face an arithmetic expression

	// Explore left expression
	ExpressionResult *leftResult = visitExpression(ctx->expression(0))
									   .as<ExpressionResult *>();

	int temporaryVariableCreated = 0; //false

	// Store the result of our left expression into our temporary variable
	string temporaryName;
	int tempSP;
	if (leftResult->getType() == TYPE_REGISTER)
	{
		// Create the temporary variable
		temporaryName = "!tmp" + to_string(++tempCount);
		tempSP = -(++varCount) * 4;
		variables.insert(make_pair(temporaryName, tempSP));
		cerr << "visitExpression, create temp var '" << temporaryName << "' in addr " << tempSP << endl;

		temporaryVariableCreated = 1; //true

		// Save the result from eax to temp
		moveRegisterToAddr(REGISTER_RESULT, tempSP);

		leftResult->setType(TYPE_VARIABLE);
		leftResult->setValue(tempSP);
	}

	// Explore right expression
	ExpressionResult *rightResult = visitExpression(ctx->expression(1))
										.as<ExpressionResult *>();

	log("expression, left and right visited : " + leftResult->toString() + " and " + rightResult->toString());
	// We now have [left] into {temp} and [right] into {result}
	string operation;

	// Result of our expression
	ExpressionResult *result = NULL;

	if (ctx->opr == nullptr)
	{
		operation = "&";
	}
	else
	{
		operation = ctx->opr->getText();
	}
	cerr << "visitExpression, Operation with '" << operation << "'" << endl;
	if (operation == "+")
	{
		result = addResults(leftResult, rightResult);
	}
	else if (operation == "-")
	{
		result = substractResults(leftResult, rightResult);
	}
	else if (operation == "*")
	{
		result = multiplyResults(leftResult, rightResult);
	}
	else if (operation == "/")
	{
		result = divideResults(leftResult, rightResult);
	}
	else if (operation == "%")
	{
		result = moduloResults(leftResult, rightResult);
	}
	else if (operation == "==")
	{
		result = compareResults(leftResult, rightResult, EQUALS);
	}
	else if (operation == "<")
	{
		result = compareResults(leftResult, rightResult, LIGHTER);
	}
	else if (operation == "<=")
	{
		result = compareResults(leftResult, rightResult, LIGHTER_OR_EQUAL);
	}
	else if (operation == ">")
	{
		result = compareResults(leftResult, rightResult, GREATER);
	}
	else if (operation == ">=")
	{
		result = compareResults(leftResult, rightResult, GREATER_OR_EQUAL);
	}
	else if (operation == "|")
	{
		result = bitWiseOperation(leftResult, rightResult, OR);
	}
	else if (operation == "^")
	{
		result = bitWiseOperation(leftResult, rightResult, XOR);
	}
	else if (operation == "&")
	{
		result = bitWiseOperation(leftResult, rightResult, AND);
	}
	else if (operation == "!=")
	{
		result = compareResults(leftResult, rightResult, NON_EQUAL);
	}

	if (temporaryVariableCreated)
	{
		cerr << "visitExpression, free temp var '" << temporaryName << "' in addr " << tempSP << endl;
		// Free temp variable :
		variables.erase(temporaryName);
		tempCount--;
		varCount--;
	}

	delete leftResult;
	delete rightResult;

	log("expression finished, return " + result->toString());

	return result;
}
