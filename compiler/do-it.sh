./compile_ubuntu.sh
cd ../tests
rm -rf pld-test-output

if [ "$#" -ne 1 ]
then
	python3 pld-test.py tests/
else
	python3 pld-test.py tests/Init/$1
fi
