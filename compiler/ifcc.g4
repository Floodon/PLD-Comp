grammar ifcc;

axiom : prog       
      ;
// vérifier avec analyseur statique que la fonction main existe
prog : declarationFonction* main ;
// prog : declarationFonction+ ;

instruction
      : declaration
      | affectationLine
      | function
      | expression
      | condition
      | block
      | loopWhile
      | loopFor
      | ret
      ;

condition 
      :'if' '(' expression ')' instruction
      |'if' '(' expression ')' instruction ELSE instruction
      ;

loopWhile 
      : 'while' '(' expression ')' block ;

loopFor 
      : 'for' '(' forStart? ';' expression? ';' forEnd? ')' block ;

forStart 
      : affectation (',' affectation)* ;

forEnd 
      : affectation (',' affectation)* ;

block : '{' instruction* '}' ;

main : 'int' 'main' '(' ')' block;

declarationFonction : 'int' IDENT '(' ')' block ;

appelFonction : IDENT '('')' (';')?;

function : IDENT '(' CONST ')' ';' ;

unaryAffectation 
      : lValue                      #unaryLValueAffectation
      | lValue ('=' expression)?    #unaryLValueExpAffectation
      ;

declaration : 'int' unaryAffectation (',' unaryAffectation)* ';' ;

affectationLine : affectation ';' ;

affectation : lValue '=' expression ;

expression 
      : notExpression
      | '-' notExpression
      | expression '&' expression
      | expression opr=('^'|'|') expression
      | expression opr=('*'|'/'|'%') expression 
      | expression opr=('+'|'-') expression 
      | expression opr=('=='|'<'|'<='|'>'|'>='|'!=') expression
      ;

notExpression
      : unaryExpression
      | '!' notExpression
      ;

lValue
      : IDENT                         
      | IDENT '[' expression ']'      
      ;

unaryExpression 
      : appelFonction                 #fonctionExpression
      | '(' expression ')'            #parExpression
      | CONST                         #constExpression
      | IDENT                         #varExpression
      | IDENT '[' expression ']'      #arrayExpression
      ;

ret : 'return' expression ';' ;

ELSE : 'else' ;

// Start by a letter and may contain numbers
IDENT : [a-zA-Z][a-zA-Z0-9]* ;

// A voir si "-?" peut être ok
CONST : [0-9]+ ;

COMMENT : '/*' .*? '*/' -> skip ;

DIRECTIVE : '#' .*? '\n' -> skip ;

WS    : [ \t\r\n] -> channel(HIDDEN);
